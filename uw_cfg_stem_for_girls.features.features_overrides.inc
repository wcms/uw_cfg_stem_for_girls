<?php

/**
 * @file
 * uw_cfg_stem_for_girls.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_stem_for_girls_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the
  // magic.
  $overrides = array();

  // Exported overrides for: field_instance.
  $overrides["field_instance.paragraphs_item-sph_remote_events_block-field_events.settings|allowed_bundles|sph_remote_events"] = 'sph_remote_events';

  return $overrides;
}
