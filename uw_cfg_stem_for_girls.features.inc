<?php

/**
 * @file
 * uw_cfg_stem_for_girls.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_cfg_stem_for_girls_field_default_field_instances_alter(&$data) {
  if (isset($data['paragraphs_item-sph_remote_events_block-field_events'])) {
    $data['paragraphs_item-sph_remote_events_block-field_events']['settings']['allowed_bundles']['sph_remote_events'] = 'sph_remote_events'; /* WAS: -1 */
  }
}
